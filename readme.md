# Gousto Code Test

A React + Redux implementation

## Running

First `npm install` to install all the necessary npm dependencies. 

Then run `npm start` and open <localhost:7770> in your browser.

## Production Build

Run `npm run-script prod` to create a distro folder and a bundle.js file.

## Tests

Run mocha './tests/**/*.spec.js' --compilers js:babel-core/register to run mocha tests.
