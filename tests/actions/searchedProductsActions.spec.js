import expect from 'expect';
import * as types from '../../client/Actions/actionTypes';  
import { setProductSearchedFor } from '../../client/Actions/searchedProductActions';

describe('Searched Products Actions', () => {

  describe('set Product searched for success',()=> {

    it('should return set product searched for success', () => {
      const expected = {
        type: types.SEARCHED_PRODUCT_NAME,
        searchedProduct: "Test"
      };

      const actual = setProductSearchedFor("Test");
      expect(actual).toEqual(expected);
    });

  });

})
