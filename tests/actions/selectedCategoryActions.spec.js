import expect from 'expect';
import * as types from '../../client/Actions/actionTypes';  
import { setSelectedCategory } from '../../client/Actions/selectedCategoryActions';

describe('Selected Category Actions', () => {

  describe('setSelectedCategory correctly',()=> {

    it('should return set selected category for success', () => {
      const expected = {
        type: types.SELECTED_CATEGORY,
        selectedCategory: "Test"
      };

      const actual = setSelectedCategory("Test");
      expect(actual).toEqual(expected);
    });

  });

})
