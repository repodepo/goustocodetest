import expect from 'expect';
import * as types from '../../client/Actions/actionTypes';  
import { loadCategoriesSuccess, loadCategories, loadProductsSuccess } from '../../client/Actions/productsActions';

describe('Products Actions', () => {

  describe('load products success',()=> {

    it('should return load products success', () => {
      const expected = {
        type: types.LOAD_PRODUCTS_SUCCESS,
        products: []
      };

      const actual = loadProductsSuccess([]);
      expect(actual).toEqual(expected);
    });

  });

})
