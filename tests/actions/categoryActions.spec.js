import expect from 'expect';
import * as types from '../../client/Actions/actionTypes';  
import { loadCategoriesSuccess, loadCategories } from '../../client/Actions/CategoryActions';

describe('Category Actions', () => {

  describe('load categories success',()=> {

    it('should return load categories success', () => {
      const expected = {
        type: types.LOAD_CATEGORIES_SUCCESS,
        categories: []
      };

      const actual = loadCategoriesSuccess([]);
      expect(actual).toEqual(expected);
    });

  });

})
