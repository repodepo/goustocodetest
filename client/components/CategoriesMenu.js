import React, {PropTypes} from 'react';  
import {connect} from 'react-redux';  
import {bindActionCreators} from 'redux';
import CategoriesList from './CategoriesList';
import * as actions from '../actions/categoryActions';

class CategoriesMenu extends React.Component {  

  render() {
    return (
      <div>
          <CategoriesList {...this.props} />
      </div>
    );
  }
}

CategoriesMenu.propTypes = {  
  categories: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps) {  
  if (state.categories.data) {
    return {
      categories: state.categories.data
    };
  } else {
    return {
      categories: [{id: '', title: ''}],
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesMenu);  
