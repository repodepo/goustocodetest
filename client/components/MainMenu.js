import React from 'react';  
import CategoriesMenu from './CategoriesMenu';
import Products from './Products';
import ProductSearchBox from './ProductSearchBox';

class MainMenu extends React.Component {  

  render() {
    return (
      <div>
          <CategoriesMenu {...this.props} />
          <div className="row" id='productSearchBox'><ProductSearchBox {...this.props} /></div>
          <Products {...this.props} />
      </div>
    );
  }
}

export default MainMenu;  
