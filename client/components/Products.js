import React, {PropTypes} from 'react';  
import {connect} from 'react-redux';  
import {bindActionCreators} from 'redux';
import ProductsList from './ProductsList';
import { browserHistory } from 'react-router';
import { store } from '../store';
import { setSelectedCategory } from '../actions/selectedCategoryActions';
import * as actions from '../actions/productsActions';

class Products extends React.Component {  

  componentWillReceiveProps(nextProps) {
    if(this.props.params.categoryTitle !== nextProps.params.categoryTitle) {
      store.dispatch(setSelectedCategory(nextProps.params.categoryTitle));
    }
  }

  render() {
    return (
      <div>
          <ProductsList products={this.props.products} selectedProducts={this.props.selectedProducts} />
      </div>
    );
  }
}

Products.propTypes = {  
  products: PropTypes.array.isRequired
};

const getVisibleProducts = (products, selectedCategory, productName) => {
 
  let filteredProducts = [];

  if (selectedCategory == '' || selectedCategory == undefined) {
    filteredProducts = products;
  }
  else {
    filteredProducts = products.filter((product)=> products[products.indexOf(product)].categories.some((category) => category.title === selectedCategory));
  }

  if (productName != '' && productName != undefined) {
    const filteredResults = filteredProducts.filter((product) => product.title.toLowerCase().trim().replace(/\W/g, '').includes(productName.toLowerCase().trim().replace(/\W/g, '')));
    return filteredResults;
  }
  else {
    return filteredProducts;
  }
}

function mapStateToProps(state, ownProps) {  
  if (state.products.data) {
      return {
        products: getVisibleProducts(state.products.data, state.selectedCategory, state.searchedProductName),
        selectedProducts: state.selectedProducts
      };
    }
    else {
      return {
        products: [{id: '', title: '', categories: []}],
        selectedProducts: []
      }
    } 
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);  
