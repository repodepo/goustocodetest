import React, {PropTypes} from 'react';
import { store } from '../store';
import { setSelectedProduct, deselectProduct } from '../actions/selectedProductActions';

class ProductsList extends React.Component {  
  
  handleOnClick(productId, e) {
    e.preventDefault();

    if (!this.props.selectedProducts.includes(productId)) {
      store.dispatch(setSelectedProduct(productId));
    }
    else {
      store.dispatch(deselectProduct(productId));
    }
  }

  showDescription(productId) {
    if (this.props.selectedProducts.includes(productId)) {
      return true;
    }
  }

  setTextDecoration(productId) {
    if (this.props.selectedProducts.includes(productId)) {
      return 'bold'
    }
    else {
      return 'normal'
    }
  }

  render() {
    return (
      (this.props.products.length > 0) ? 
      <div className="row p-2" id="productsRow">
        {this.props.products.map(product => 
          <div className="col-4 p-2"  
          style={ {
            fontWeight: this.setTextDecoration(product.id) 
          }} 
          key={product.id}>
            <a onClick={this.handleOnClick.bind(this, product.id)}>{product.title}</a>
            <br/>
            <br/>
            <div className={this.showDescription(product.id) ? "m-2 p-2 productsBackground" : "m-2"} 
                style={ {
                  fontWeight: 'normal'
                }} >{this.showDescription(product.id) && product.description}</div>
          </div>
        )}
      </div> : <div>No products found with that description</div>
    );
  }
}

ProductsList.propTypes = {  
  products: PropTypes.array.isRequired
};

export default ProductsList;  