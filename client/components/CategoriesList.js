import React, {PropTypes} from 'react';
import {connect} from 'react-redux';  
import {bindActionCreators} from 'redux';
import {Link} from 'react-router';
import { push } from 'react-router-redux'
import { store } from '../store';
import { setSelectedCategory } from '../actions/selectedCategoryActions';

class CategoriesList extends React.Component {  

  render() {
    return (<div className="row p-2" id="categoriesRow">
          {this.props.categories.map(category => 
            <div className="col p-2 mt-4" key={category.id} style={ {
              fontSize: '16'  
            }} >
              <Link to={'/category/' + category.title} {...this.props} onClick={this.handleOnClick}>{category.title}</Link>
            </div>
          )}
        </div>);
  }

  handleOnClick(e) {
    e.preventDefault();
    store.dispatch(setSelectedCategory(e.target.text));
    store.dispatch(push(`/category/${e.target.text}`));
  }
};

export default CategoriesList;
