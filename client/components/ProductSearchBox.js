import React, {PropTypes} from 'react';  
import {connect} from 'react-redux';  
import {bindActionCreators} from 'redux';
import ProductsList from './ProductsList';
import { browserHistory } from 'react-router';
import { store } from '../store';
import { setProductSearchedFor } from '../actions/searchedProductActions';

class ProductSearchBox extends React.Component {  
  
  handleChange(event) {
    store.dispatch(setProductSearchedFor(event.target.value));
  }

  render() {
    return (
      <div className="p-4">
          <input type="text" value={this.props.productName} placeholder={'Search for a product'} onChange={this.handleChange}></input>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {  
  if (state.searchedProduct) {
    return { searchedProduct: state.searchedProduct }
  } else {
    return {
      searchedProduct: ''
    }
  }
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}

const FilterLink = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductSearchBox)

export default ProductSearchBox;
