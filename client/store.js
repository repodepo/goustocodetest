import {createStore, applyMiddleware} from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from './reducers/rootReducer';
import thunk from 'redux-thunk';

export const store = createStore(
    rootReducer,
    applyMiddleware(
      thunk,
      routerMiddleware(browserHistory)
    )
  );
