/*eslint-disable import/default */
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { store } from './store';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
import { loadCategories } from './actions/categoryActions';
import { loadProducts } from './actions/productsActions';
require('bootstrap');
import "./styles/main.css";
  
store.dispatch(loadCategories());
store.dispatch(loadProducts());

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root')
);