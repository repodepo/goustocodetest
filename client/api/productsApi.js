class productsApi {  
    static getAllProducts() {
      return fetch('https://api.gousto.co.uk/products/v2.0/products?includes[]=categories&includes[]=attributes&sort=position&image_sizes[]=365&image_sizes[]=400&period_id=120').then(response => {
        return response.json();
      }).catch(error => {
        return error;
      });
    }
}

export default productsApi;