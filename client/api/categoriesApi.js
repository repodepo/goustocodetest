class categoriesApi {  
    static getAllCategories() {
      return fetch('https://api.gousto.co.uk/products/v2.0/categories').then(response => {
        return response.json();
      }).catch(error => {
        return error;
      });
    }
}

export default categoriesApi;