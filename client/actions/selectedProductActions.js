import * as types from './actionTypes';  

/* Selected Product */

export function setSelectedProduct(productId) {
  return {
    type: types.SELECTED_PRODUCT,
    productId
  }
}

export function deselectProduct(productId) {
  return {
    type: types.DESELECTED_PRODUCT,
    productId
  }
}