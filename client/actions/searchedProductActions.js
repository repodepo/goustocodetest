import * as types from './actionTypes';  

/* Selected Category */

export const setProductSearchedFor = searchedProduct => {
  return {
    type: types.SEARCHED_PRODUCT_NAME,
    searchedProduct
  }
}