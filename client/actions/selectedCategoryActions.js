import * as types from './actionTypes';  

/* Selected Category */

export const setSelectedCategory = selectedCategory => {
  return {
    type: types.SELECTED_CATEGORY,
    selectedCategory
  }
}