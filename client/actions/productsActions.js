import * as types from './actionTypes';  
import productsApi from '../api/productsApi';

/* Products */

export function loadProductsSuccess(products) {  
    return {type: types.LOAD_PRODUCTS_SUCCESS, products};
  }

export function loadProducts() {
  return function(dispatch) {
    return productsApi.getAllProducts().then(products => {
      dispatch(loadProductsSuccess(products));
    }).catch(error => {
      throw(error);
    });
  };
}