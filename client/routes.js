import React from 'react';
import App from './components/App';
import { Route, IndexRoute } from 'react-router';
import MainMenu from './components/MainMenu';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={MainMenu} />
    <Route path="/category/:categoryTitle" component={MainMenu} />
  </Route>
);
