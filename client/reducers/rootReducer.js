import { combineReducers } from 'redux';

/*
  Reducers

  Reducers match up the dispatched (fired) action with a function that should be called.

  It takes in a copy of state, modifies it, and returns the new state
  When state gets large, it makes sense to have multiple reducers that only deal with a piece of the state

*/

import { routerReducer } from 'react-router-redux'; 
import categories from './categoriesReducer';
import products from './productsReducer';
import selectedCategory from './selectedCategoryReducer';
import searchedProductName from './searchedProductReducer';
import selectedProducts from './selectedProductsReducer';

// Combine all our reducers togeher
const rootReducer = combineReducers({ categories, products, selectedCategory, searchedProductName, selectedProducts, routing: routerReducer });

export default rootReducer;
