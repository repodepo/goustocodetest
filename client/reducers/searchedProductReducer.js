import * as types from '../actions/actionTypes';  
import initialState from './initialState';

export default function searchedProductReducer(state = initialState.searchedProduct, action) {  
  switch(action.type) {
    case types.SEARCHED_PRODUCT_NAME:
      return action.searchedProduct;
    default: 
      return state;
  }
}