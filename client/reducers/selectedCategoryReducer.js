import * as types from '../actions/actionTypes';  
import initialState from './initialState';

export default function selectedCategoryReducer(state = initialState.selectedCategory, action) {  
  switch(action.type) {
    case types.SELECTED_CATEGORY:
      return action.selectedCategory;
    default: 
      return state;
  }
}