import * as types from '../actions/actionTypes';  
import initialState from './initialState';

export default function selectedProductsReducer(state = initialState.selectedProducts, action) {  
  switch(action.type) {
    case types.SELECTED_PRODUCT:
      return [...state, 
         action.productId
      ];
    case types.DESELECTED_PRODUCT:
      const newState = Object.assign([], state);
      const indexOfSelectedProductToDelete = state.findIndex(selectedProductToDelete => {return selectedProductToDelete.productId == action.productId})
      newState.splice(indexOfSelectedProductToDelete, 1);
      return newState;
    default:
      return state;
  }
}